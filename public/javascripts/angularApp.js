var app = angular.module('flapperNews',['ui.router']);

app.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider){
	$stateProvider
		.state('home',
			{
				url: '/home',
				templateUrl: '/home.html',
				controller: 'MainController'
			})
		.state('posts',
			{
				url: '/posts/{id}',
				templateUrl: '/posts.html',
				controller: 'PostController'
			});

	$urlRouterProvider.otherwise('home');
}]);