app.controller('PostController',['$scope','$stateParams','posts',function($scope,$stateParams,posts){
	$scope.post = posts.items[$stateParams.id];

	$scope.addComment = function(){
		if($scope.body === ''){return;}
		$scope.post.comments.push({
			body: $scope.body,
			author: 'user',
			upvotes: 0
		});
		$scope.body = '';
	};
}]);